#!/bin/bash

PKG=plink2
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
basename=$(basename "$0")
BASE=$(realpath "${DIR}/../lib/$PKG/${basename}")

function test_and_run () {
	if grep -q "$1" /proc/cpuinfo && [ -x "${BASE}-$1" ]; then
		cmd="${BASE}-$1"
		shift
		"${cmd}" "$@"
		exit
	fi
}

function test_and_run_avx2 () {
	if cat /proc/cpuinfo \
	   | grep avx2 \
	   | grep bmi \
	   | grep bmi2 \
	   | grep abm \
	   | grep fma \
	   > /dev/null \
	   && [ -x "${BASE}-avx2" ]; then
		cmd="${BASE}-avx2"
		"${cmd}" "$@"
		exit
	fi
}

test_and_run_avx2 "$@"

for SIMD in avx sse4.1 ssse3 sse3 sse2 sse mmx ; do test_and_run ${SIMD} "$@" ; done

# fallback to plain option
set -x
"$BASE-plain" "$@"
